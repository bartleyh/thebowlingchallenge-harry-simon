
class Bowling(object):

    def __init__(self):
        self.frame = 1
        self.score_board = {}
        self.keepscore = {}
        self.totalscore = 0

    def input_score(self):
        while self.getframe() < 11:
            print("This is frame", self.getframe())
            score1 = input("Please insert score 1 (insert number 1-9 or X): ")
            if self.isStrike(score1):
                self.score_board[self.frame] = ["X", ""]
            else:
                score2 = input("Please insert score 2 (insert number 1-9 ): ")
                if self.isSpare(score1, score2):
                    self.score_board[self.frame] = [score1, "/"]
                else:
                    self.score_board[self.frame] = [score1, score2, self.get_totalscore()]
            self.show_score_board()
            self.frame += 1


    def isSpare(self, score1, score2):
        total = int(score1) + int(score2)
        if total == 10:
            print("Nice spare!")
            return True
        else:
            return False


    def isStrike(self, score):
        if score == "X":
            print("Lovely strike!")
            return True
        else:
            return False


    def show_score_board(self):
        print(self.get_score_board())

    def getframe(self):
        return self.frame

    def get_score_board(self):
        return self.score_board

    def get_totalscore(self):
        return self.totalscore

if __name__ == "__main__":
    b = Bowling()
    b.input_score()
